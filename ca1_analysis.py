"""
CA1 ECG analysis

Student name: Hongxu Liu
"""

import sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
# imports go here (anything in numpy/scipy is fine)

# re-usable functions / classes go here

def analyse_ecg(signal_filename, annotations_filename):
    # basic formatted output in Python
    print("\nsignal filename: %s" % signal_filename)
    print("annotation filename: %s" % annotations_filename + "\n")
    
    # your analysis code here  
    
    data = np.loadtxt(signal_filename,delimiter=",",skiprows=2)
    # get Elapsed times in arraylist
    dataTimes = tuple(x[0] for x in data)
    #time length 
    timeLength = dataTimes[-1] - dataTimes[0]
    #simple rate
    samplingRate = len(data) / timeLength
    #data length
    dataLength = len(data)
    #get the min and max vaule in signal
    signalECG1 = tuple(y[1] for y in data)
    signalECG2 = tuple(z[2] for z in data)
    maxECG1 = max(signalECG1)
    minECG1 = min(signalECG1)
    maxECG2 = max(signalECG2)
    minECG2 = min(signalECG2)
    
    #count lines in input1_annotations.txt file(except the first line)
    num_lines = sum(1 for line in open(annotations_filename))-1

    print("Sampling Rate: %3.0f" % samplingRate + "Hz")
    print("Data Length: %s" %dataLength + " samples")
    print("Time Length: %2.0f" %timeLength + " s")
    if maxECG1 > maxECG2:
        print("Max signal value: %s" %maxECG1 + " mV")
    else:
        print("Max signal value: %s" %maxECG2 + " mV") 
    if minECG1 < minECG2:
        print("Min signal value: %s" %minECG1 + " mV")
    else:
        print("Min signal value: %s" %minECG2 + " mV")
    print("Number of R waves in annotation file: %i" %num_lines)
    
    #get x and y from file
    rc('text' , usetex=True)
    dataMatrix1 = np.genfromtxt(signal_filename)
    #set x.y value
    x = dataMatrix1[:0]
    y1 = dataMatrix1[:1]
    y2 = dataMatrix1[:2]
    #set two line lables
    plot(x,y1,label=r'ECG1')
    plot(x,y2,label=r'ECG2')
    legend(loc= 'upper right')
    #set x,y lable
    xlabel(r'$x$')
    plt.ylabel(r'$time$')
    show()    
    
    
if __name__=="__main__":
    #analyse_ecg(sys.argv[1] + "_signals.txt", sys.argv[1] + "_annotations.txt")
    analyse_ecg("input1" + "_signals.txt", "input1"+"_annotations.txt")

