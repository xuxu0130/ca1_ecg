CA1
===

Modify this file to complete the following questions.
Other than filling in required information, please *DO NOT* make any changes to this file's structure or the question text (to ease grading).

Student information
-------------------

Name (as on Moodle): Hongxu Liu

Sortable name (no spaces, surname then firstname): liu_hongxu

Reflection questions (after coding)
-----------------------------------

When you have finished coding, please reflect briefly on the following three questions. Marks here are awarded for engaging with the question - they're not a trick, and there is no "right" answer.

Question 1
^^^^^^^^^^

If you had much more time to work on this problem, how would you attempt to improve your code? (Suggested length: one short paragraph)

YOUR ANSWERE HERE.

Making the code clear and easier and find the way to solve all the problems. Try different ways and find which one may be the better one
easy to understand and get the right answer. Code not that hard to write but should pay more patient on it. 


Question 2
^^^^^^^^^^

What is the most important thing that you learned from this lab exercise? (Suggested length: one sentence)

YOUR ANSWERE HERE.

Don't hesitate to ask when confused and share idea may be the good way to improve the skill and efficiency.

Question 3
^^^^^^^^^^

What did you like/dislike the most about this lab exercise? (Suggested length: one sentence)

YOUR ANSWER HERE.
The more exercise done the more way will get to solve the problem, it takes time but worth it.
